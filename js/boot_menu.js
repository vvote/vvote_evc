/**
 *   This file is part of vVote from the Victorian Electoral Commission.
 *
 *   vVote is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License.
 *
 *   vVote is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with vVote.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Contact Craig Burton   craig.burton@vec.vic.gov.au
 *
 *
 *  The boot menu currently allows the user to select what "app" this device will be.
 *  It performs validation checks on the JSON configuration data to ensure that it meets basic content and format requirements. 
 * 
 * @author Peter Scheffer
 */

var languageFileIsValid = false;
var audioFileIsValid = false;
var audioFileListingIsValid = false;
var mediaFileIsValid = false;
var mediaFileListingIsValid = false;
var stagingFileIsValid = false;
var candidateFileIsValid = false;

var hasFinishedCheckingAudioConfig = false;
var hasFinishedCheckingAudioFiles = false;
var hasFinishedCheckingLanguageConfig = false;
var hasFinishedCheckingMediaConfig = false;
var hasFinishedCheckingMediaFiles = false;
var hasFinishedCheckingStagingConfig = false;
var hasFinishedCheckingCandidateConfig = false;

var errorTitle = "The configuration data downloaded are damaged or missing information. Please contact Help Desk.";

function checkIsAllValid () {

  if (hasFinishedCheckingAudioConfig &&
      hasFinishedCheckingAudioFiles &&
      hasFinishedCheckingLanguageConfig &&
      hasFinishedCheckingMediaConfig &&
      hasFinishedCheckingMediaFiles &&
      hasFinishedCheckingStagingConfig &&
      hasFinishedCheckingCandidateConfig) {

    if ((languageFileIsValid == true) &&
        (audioFileIsValid == true) &&
        (audioFileListingIsValid == true) &&
        (mediaFileIsValid == true) &&
        (mediaFileListingIsValid == true) &&
        (stagingFileIsValid == true) &&
        (candidateFileIsValid == true)) {
      displayMessage("Finished Validation.", "application is ready to start.");
    } else {
//      displayError(errorTitle, "Unknown error. Files didn't validate.");
    }
  }
}

function checkBundle () {

  $("#check_bundle_button").hide();

  $("#message").html("Please wait while we validate the configuration files.");
  $("#message").show();

  window.setTimeout(function () {
    $("#message").html("If there are no errors below, then the application is ready to run.");
  }, 20000);

  languageFileIsValid = false;
  audioFileIsValid = false;
  audioFileListingIsValid = false;
  mediaFileIsValid = false;
  mediaFileListingIsValid = false;
  stagingFileIsValid = false;
  candidateFileIsValid = false;

  hasFinishedCheckingAudioConfig = false;
  hasFinishedCheckingAudioFiles = false;
  hasFinishedCheckingLanguageConfig = false;
  hasFinishedCheckingMediaConfig = false;
  hasFinishedCheckingMediaFiles = false;
  hasFinishedCheckingStagingConfig = false;
  hasFinishedCheckingCandidateConfig = false;

  getMediaData(); 
}

$(document).ready(function () {

  $(document).bind("finishedCheckingMediaConfig", function (event, mediaData) {
    hasFinishedCheckingMediaConfig = true;
    if (mediaData != null) {
      try {
        checkMediaFilePresence(mediaData);
      } catch (error) {
        displayError(errorTitle, error);
      }
    }
  });

  $(document).bind("finishedCheckingMediaFiles", function () {
    hasFinishedCheckingMediaFiles = true;
    getAudioConfiguration();
  });

  $(document).bind("finishedCheckingAudioConfig", function (event, audioData) {
    hasFinishedCheckingAudioConfig = true;
    if (audioData != null) {
      try {
        var audioArray = audioData["clips"];
        checkAudioFilePresence(audioArray);
      } catch (error) {
        displayError(errorTitle, error);
      }
    }
  });

  $(document).bind("finishedCheckingAudioFiles", function () {
    hasFinishedCheckingAudioFiles = true;
    getStagingConfigData();
  });

  $(document).bind("finishedCheckingStagingConfig", function () {
    hasFinishedCheckingStagingConfig = true;
    getNominationsData();
  });

  $(document).bind("finishedCheckingCandidateConfig", function () {
    hasFinishedCheckingCandidateConfig = true;
    getLanguageData();
  });

  $(document).bind("finishedCheckingLanguageConfig", function () {
    hasFinishedCheckingLanguageConfig = true;
    checkIsAllValid();
  });
  
  $(document).bind("error", function (event, error) {
    displayError(errorTitle, error);
  });
});

// Get all language data configuration information.
function getLanguageData() {

  try {
      
    var randomSeed = new Date().getTime();  
    $.getJSON('data/language_config.txt?t=' + randomSeed, function(languageData) {
      
      // Validate the JSON with a schema.
      $.ajax({
        type: 'GET',
        url: 'validation/language_config_schema.txt?t=' + randomSeed,
        data: '',
        cache: false,
        dataType: 'json',
        success: function (schemaFile) {
          var valid = tv4.validate(languageData, schemaFile);
          if (!valid) {
            displayError(errorTitle, "Missing key for: " + tv4.error.dataPath + "<br/>" + tv4.error.message + "<br/>in language configuration file.");
          } else {
            languageFileIsValid = true;
          }
          $(document).trigger("finishedCheckingLanguageConfig");         
        },        
        error: function (xhr, status, error) {
          alertFailure(xhr, error.message);
          $(document).trigger("finishedCheckingLanguageConfig");
          return false;
        }
      });
    })
    .error(function () { 
      displayError(errorTitle, 'ERROR: JSON Language Data is missing or not valid.'); 
      $(document).trigger("finishedCheckingLanguageConfig");
      return false;
    });
  } catch (error) {
    displayError(errorTitle, "Error reading language data: " + error);
    $(document).trigger("finishedCheckingLanguageConfig");
    return false;
  }

  return true;
}

// Get all media data configuration information.
function getMediaData() {

  try {
      
    var randomSeed = new Date().getTime();
    $.getJSON('data/media_config.txt?t=' + randomSeed, function(mediaData) {

      // Validate the JSON with a schema.
      $.ajax({
        type: 'GET',
        url: 'validation/media_config_schema.txt?t=' + randomSeed,
        data: '',
        cache: false,
        dataType: 'json',
        success: function (schemaFile) {
          var valid = tv4.validate(mediaData, schemaFile);
          if (!valid) {
            displayError(errorTitle, "Missing key for: " + tv4.error.dataPath + "<br/>" + tv4.error.message + "<br/>in media configuration file.");
          } else {
            mediaFileIsValid = true;            
          }
          $(document).trigger("finishedCheckingMediaConfig", mediaData);
        },        
        error: function (xhr, status, error) {
          alertFailure(xhr, error.message);
          $(document).trigger("finishedCheckingMediaConfig");
          return false;
        }
      });
    })
    .error(function () { 
      displayError(errorTitle, 'ERROR: JSON Media Data is missing or not valid.'); 
      $(document).trigger("finishedCheckingMediaConfig");
      return false;
    });
  } catch (error) {
    displayError(errorTitle, "Error reading media data: " + error);
    $(document).trigger("finishedCheckingMediaConfig");
    return false;
  }

  return true;
}

// Get all audio clip configuration information.
function getAudioConfiguration() {

  try {
      
    var randomSeed = new Date().getTime();
    // Get the JSON audio configuration file.
    $.getJSON('data/audio_config.txt?t=' + randomSeed, function(audioData) {

      // Validate the JSON with a schema.
      $.ajax({
        type: 'GET',
        url: 'validation/audio_config_schema.txt?t=' + randomSeed,
        data: '',
        cache: false,
        dataType: 'json',
        success: function (schemaFile) {
          var valid = tv4.validate(audioData, schemaFile);
          if (!valid) {
            displayError(errorTitle, "Missing key for: " + tv4.error.dataPath + "<br/>" + tv4.error.message + "<br/>in audio configuration file.");
          } else {
            audioFileIsValid = true;
          }
          
          $(document).trigger("finishedCheckingAudioConfig", [audioData]);
        },        
        error: function (xhr, status, error) {
          alertFailure(xhr, error.message);
          audioFileIsValid = false;
          $(document).trigger("finishedCheckingAudioConfig");
          return false;
        }
      });
    })
    .error(function () { 
      displayError(errorTitle, 'ERROR: JSON Audio Configuration is missing or not valid.'); 
      audioFileIsValid = false;
      $(document).trigger("finishedCheckingAudioConfig");
      return false;
    });
  } catch (error) {
    displayError(errorTitle, "Error reading audio configuration: " + error);
    audioFileIsValid = false;
    $(document).trigger("finishedCheckingAudioConfig");
    return false;
  }
  
  return true;
}

// Get all nominations data.
function getNominationsData() {
  
  try {
      
    var randomSeed = new Date().getTime();
    // Get the JSON nominations file.
    $.getJSON('bundle/region_district_candidate_data.txt?t=' + randomSeed, function(nominationsData) {

      // Validate the JSON with a schema.
      $.ajax({
        type: 'GET',
        url: 'validation/ballot_draw_schema.txt?t=' + randomSeed,
        data: '',
        cache: false,
        dataType: 'json',
        success: function (schemaFile) {
          var valid = tv4.validate(nominationsData, schemaFile);
          if (!valid) {
            displayError(errorTitle, "Missing key for: " + tv4.error.dataPath + "<br/>" + tv4.error.message + "<br/>in nominations candidate data file.");
          } else {
            candidateFileIsValid = true;
          }
          $(document).trigger("finishedCheckingCandidateConfig");
        },        
        error: function (xhr, status, error) {
          alertFailure(xhr, error.message);
          $(document).trigger("finishedCheckingCandidateConfig");
          return false;
        }
      });
    })
    .error(function (error) { 
      displayError(errorTitle, 'ERROR: JSON Nominations data file is missing or not valid: ' + error); 
      $(document).trigger("finishedCheckingCandidateConfig");
      return false;
    });
  } catch (error) {
    displayError(errorTitle, "Error reading nominations data file: " + error);
    $(document).trigger("finishedCheckingCandidateConfig");
    return false;
  }
  
  return true;
}

// Get all staging configuration information.
function getStagingConfigData() {

  try {
      
    var randomSeed = new Date().getTime();  
    $.getJSON('stagingconfig.json?t=' + randomSeed, function(stagingData) {
      
      // Validate the JSON with a schema.
      $.ajax({
        type: 'GET',
        url: 'validation/staging_config_schema.txt?t=' + randomSeed,
        data: '',
        contentType: 'application/json',
        cache: false,
        dataType: 'json',
        success: function (schemaFile) {
          var valid = tv4.validate(stagingData, schemaFile);
          if (!valid) {
            displayError(errorTitle, "Missing key for: " + tv4.error.dataPath + "<br/>" + tv4.error.message + "<br/>in staging configuration file.");
          } else {
            stagingFileIsValid = true;
          }
          $(document).trigger("finishedCheckingStagingConfig");
        },        
        error: function (xhr, status, error) {
          alertFailure(xhr, error.message);
          $(document).trigger("finishedCheckingStagingConfig");
          return false;
        }
      });
    })
    .error(function () { 
      displayError(errorTitle, 'ERROR: JSON Staging Configuration Data is missing or not valid.'); 
      $(document).trigger("finishedCheckingStagingConfig");
      return false;
    });
  } catch (error) {
    displayError(errorTitle, "Error reading staging configuration data: " + error);
    $(document).trigger("finishedCheckingStagingConfig");
    return false;
  }

  return true;
}

// Check that every audio file is present and non-empty.
function checkAudioFilePresence (audioArray) {
    
  var valid = true;
    
  for (key in audioArray) {
    var content = audioArray[key];
    if (typeof content === 'string') {
      try {
        valid = checkForFile(content);
      } catch (error) {
        throw error;      
      }
    } else if (typeof content === 'object') {
      for (key2 in content) {
        var content2 = content[key2];
        if (typeof content2 === 'string') {
          try {
            valid = checkForFile(content2);
          } catch (error) {
            throw error;      
          }
        }
      }
    }
  }
  
  $(document).trigger("finishedCheckingAudioFiles");
  return valid;
}

// Check that every image/video file is present and non-empty.
function checkMediaFilePresence (mediaArray) {
    
  var imagesArray = mediaArray["images"];
  for (key in imagesArray) {
    var filename = imagesArray[key]["filename"];
    if (typeof filename === 'string') {
      try {
        checkForFile(filename);
      } catch (error) {
        throw error;
      }
    }
  }

  var videosArray = mediaArray["videos"];
  for (key in videosArray) {
    var filename = videosArray[key]["filename"];
    if (typeof filename === 'string') {
      try {
        checkForFile(filename);
      } catch (error) {
        throw error;
      }
    }
  }
  
  $(document).trigger("finishedCheckingMediaFiles");
}

// Check header of media files for presence/non-zero length.
function checkForFile (fileUrl) {
try {
  var xhr = $.ajax({
    type: "HEAD",
    async: true,
    url: fileUrl,
    success: function(message, text, response) {
      var fileLength = xhr.getResponseHeader('Content-Length');
      if (fileLength == 0) {
        $(document).trigger("error", 'File is zero length: ' + fileUrl);
        return false;
      }
    },
    error: function (error) {
      $(document).trigger("error", 'File is missing: ' + fileUrl);
    }
  });
} catch (error) {
  $(document).trigger("error", error);
}
}

function alertFailure (xhr, error) {
  if (xhr.statusText == "OK") {
    if (xhr.isRejected()) {
      displayError(errorTitle, "JSON was rejected: " + error);
    }
  } else {
    displayError(errorTitle, error);
  }
}

function displayMessage (title, message) {
  $('#error_heading').html(title);
  $('#error_message').html(message);
  $('#error_message_popup').show();
}

function hideMessage () {
  $('#error_message_popup').hide();
  $("#check_bundle_button").show();
}

function displayError (title, message) {
  $('#error_heading').html(title);
  $('#error_message').html(message);
  $('#error_message_popup').show();
}

function hideError () {
  $('#message').hide();
  $('#error_message_popup').hide();
  $("#check_bundle_button").show();
}

function hidePolling () {
  $('#polling_message_popup').hide();
}

function showPolling (message) {
  $('#polling_message_popup').show();
  $('#polling_message').html(message);
}
