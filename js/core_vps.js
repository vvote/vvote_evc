/**
 *   This file is part of vVote from the Victorian Electoral Commission.
 *
 *   vVote is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License.
 *
 *   vVote is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with vVote.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Contact Craig Burton   craig.burton@vec.vic.gov.au
 *
 *
 * Core functionality to drive the VPS application.
 * 
 * @author Peter Scheffer
 */

var scanTimeout = null;

var currentQrCode = null;

// Should we be logging user actions?
var useLogging = false;

// The configuration data specific to languages.
var languagesConfig = new Array();

// The range of available languages.
var languageOptions = new Array();

// Default language.
var languageSelection = "english";

// Language before switch.
var previousLanguageSelection = null;

// The list of language dictionaries, according to language.
var dictionaryOptions = new Array();

// The list of onscreen elements that need conversion.
var textForConversionArray = new Array();

var alertDelayedTimeout;

var DELAYED_TIMEOUT_PERIOD = 5000;

var barcodeTimeout = null;

var displayAuditTab = false;

// Get the current language selection.
function getCurrentLanguageSelection () {
  return languageSelection;
}

function setCurrentLanguageSelection (language) {
  languageSelection = language;
}

function getStagingConfiguration () {

  try {
    var randomSeed = new Date().getTime();    
    $.getJSON('stagingconfig.json?t=' + randomSeed, function(stagingData) {
      displayAuditTab = stagingData["VPS_Audit"];

      if (displayAuditTab == null) {
        displayAuditTab = true;
      }  

      if (displayAuditTab == false) {
        hideAuditTab();
      }
    })
    .error(function () { alert('ERROR: Staging Config data is not valid.'); });
  } catch (error) {
    alert("Error staging configuration data: " + error);
  }
}

function hideAuditTab () {
  $("#audit_candidate_list").hide();
  $("#audit_candidate_list_button").hide();
}

/**
 * Load audio configuration data into the application.
 */
// Get all audio clip configuration information.
function getAudioConfiguration() {
    try {
        var randomSeed = new Date().getTime();
        $.getJSON('data/audio_config.txt?t=' + randomSeed, function (data) {
            $(document).trigger("receivedAudioConfig", data);
        })
        .error(function () { alert('ERROR: JSON Audio Configuration is not valid.'); });
    } catch (error) {
        alert("Error reading audio configuration: " + error);
    }
}

// Once we have the data and the document is ready, load the audio array.
function receivedAudioConfig(event, data) {
  audioArray = data["clips"];
}

/**
 * Load language data into the application.
 */
// Get all language data.
function getLanguageData() {
    try {
        var randomSeed = new Date().getTime();
        $.getJSON('data/language_config.txt?t=' + randomSeed, function (data) {
            $(document).trigger("receivedLanguageConfig", data);
        })
        .error(function () { alert('ERROR: JSON Language Data is not valid.'); });
    } catch (error) {
        alert("Error reading language data: " + error);
    }
}

// Once we have the data and the document is ready, load the language dictionaries.
function receivedLanguageConfig(event, data) {

  languagesConfig = data["languages"];
  var index = 1;
  for (key in languagesConfig) {
    var language = languagesConfig[key]["language"];
    $("#language").append("<option value='" + index + "'>" + languagesConfig[key]["englishName"] + "</option>");
    languageOptions[index++] = language;
    
    var language_dictionary = languagesConfig[key]["dictionary"];
    for (key2 in data["dictionaries"]) {
      var dictionary = data["dictionaries"][key]["dictionary"];
      if (dictionary == language_dictionary) {
        dictionaryOptions[language] = data["dictionaries"][key]["translations"];
      }
    }
    
    if (language != null && dictionaryOptions[language] == null) {
      alert("Dictionary missing in configuration file for " + language);
      return;
    }
  }
  
  var textLabelsForConversion = data["conversion_labels"];
  for (key in textLabelsForConversion) {
    textForConversionArray.push(key);
  }
}

$(document).ready(function() {

  // Configure event listeners.  We need to trigger an event to load the candidate
  // data at a time that coincides with the document and JSON data being loaded.
  $(document).bind("receivedAudioConfig", receivedAudioConfig);
  $(document).bind("receivedLanguageConfig", receivedLanguageConfig);

  // Get the configuration data.
  getAudioConfiguration();
  getLanguageData();
});

function getBarcodeForQuarantine() {
    //showQuarantineConfirmation("Broadmeadows;Northern Metropolitan;2500101:19;:000201:1511091308211728011023002414060426051219202516180227030722;AxEXcI3NWWGP8T1F1jWqotkNfJ0hl6vBYA1pp3I6WanXUToPHxobQQ==;1;1;1;1;1;0");
    $.ajax({
        type: 'POST',
        timeout: SCAN_TIMEOUT,
        url: "/servlet/getBarcode",
        success: function (response) {
            showQuarantineConfirmation(response);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus === "timeout") {
                getBarcodeForQuarantine();
            } else if (jqXHR.status == 500) {
                getBarcodeForQuarantine();
            } else {
                //        noWebcam('unknown webcam error occurred: ' + textStatus + ':' + errorThrown + ':' + jqXHR.responseText);
            }
        }
    });
}

function showQuarantineConfirmation (qrCode) {
  currentQrCode = qrCode;
  $('#confirm_quarantine').show();
  $('#quarantine_main').hide();
}

function handleQuarantine () {

  // qrCode comes null if no code were detected
  if (currentQrCode === null) { //expect a lot of nulls
    // Do nothing
  } else {
    
    window.clearTimeout(scanTimeout);  
    scanTimeout = null;
      
    var qrData = currentQrCode.split(";");
    if (qrData.length != 11) {
      showInvalidQrAlert();
      return;
    }
    
    var district = qrData[0];
    var serialNo = qrData[2];
    var signature = qrData[4];

    var clips = [ ['alert_scanned_okay'] ];
    var instructions = new AudioInstructions(clips, false, true, null);
    var container = getContainer();
    var audioController = container.Resolve("audioController");
    audioController.playAudioInstructions(instructions);

    var msg = {};
    msg.type = "cancelreq";
    msg.serialNo = serialNo;
    msg.serialSig = signature;
    msg.district = district;

    var cancelSlipMessage = "msg=" + encodeURIComponent(JSON.stringify(msg));

    var cancelURL = "servlet/CancelAuthProxy";

    $.ajax({
        type: 'POST',
        timeout: MBB_TIMEOUT,
        url: cancelURL,
        data: cancelSlipMessage,
        dataType: "json",
        success: function (response) {

            // check response 
            if (response == null) {
                showQuarantineFailure('Error: Incorrect response');
            } else {
                // Check for errors.
                var error = response["ERROR"];
                if (error != null && error != "") {
                    showQuarantineFailure('Error: ' + error);
                } else if (response["Error"] != null && response["Error"] != "") {
                    showQuarantineFailure('Error: ' + response["Error"]);
                }
                else {
                    msg.cancelAuthID = response.Auth.cancelAuthID;
                    msg.cancelAuthSig = response.Auth.cancelAuthSig;
                    showQuarantineSuccess(msg);
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            showQuarantineFailure(NETWORK_ERROR_MESSAGE + "VVA");
        }
    });
  }
}

function showQuarantineSuccess(msg) {

    if (currentQrCode === null) {
        // Do nothing
    } else {

        window.clearTimeout(scanTimeout);
        scanTimeout = null;

        var qrData = currentQrCode.split(";");
        if (qrData.length != 11) {
            showInvalidQrAlert();
            return;
        }

        var clips = [['alert_scanned_okay']];
        var instructions = new AudioInstructions(clips, false, true, null);
        var container = getContainer();
        var audioController = container.Resolve("audioController");
        audioController.playAudioInstructions(instructions);

        msg.type = "cancel";
        var auditSlipMessage = "msg=" + encodeURIComponent(JSON.stringify(msg));
        var auditURL = "servlet/MBBPODProxy";

        $.ajax({
            type: 'POST',
            timeout: MBB_TIMEOUT,
            url: auditURL,
            data: auditSlipMessage,
            dataType: "json",
            success: function (response) {

                // check response 
                if (response == null) {
                    showQuarantineFailure('Error: Incorrect response');
                } else {
                    // Check for errors.
                    var error = response["ERROR"];
                    if (error != null && error != "") {
                        showQuarantineFailure('Error: ' + error);
                    } else {
                        $('#quarantine_wait').hide();
                        $('#quarantine_success').show();

                        // Generate the Quarantine receipt.
                        var region = districtRegionArray[msg.district];
                        var languageVal = $('#language').val();
                        var fontSize = $('#fontsize').val();
                        var contrast = $('#contrast').val();
                        var interface = $('#interface').val();

                        buildQuarantine(msg.district, region, msg.serialNo, msg.serialSig, response);
                    };
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                showQuarantineFailure(NETWORK_ERROR_MESSAGE + "MBB");
            }
        });
    }
}

function getBarcodeForAudit() {
    //{
    //    var response = "Broadmeadows;Northern Metropolitan;2500101:19;:000201:1511091308211728011023002414060426051219202516180227030722;AxEXcI3NWWGP8T1F1jWqotkNfJ0hl6vBYA1pp3I6WanXUToPHxobQQ==;1;1;1;1;1;0";
    //    var qrData = response.split(";");
    //    var serialCode = qrData[2];
    //    $("#audit_serial_number").html(serialCode);
    //    $("#audit_button").show();
    //    currentQrCode = response;
    //}
    $.ajax({
        type: 'POST',
        timeout: SCAN_TIMEOUT,
        url: "/servlet/getBarcode",
        success: function (response) {
            var qrData = response.split(";");
            var serialCode = qrData[2];
            $("#audit_serial_number").html(serialCode);
            $("#audit_button").show();
            currentQrCode = response;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            if (textStatus === "timeout") {
                getBarcodeForAudit();
            } else if (jqXHR.status == 500) {
                getBarcodeForAudit();
            } else {
                //        noWebcam('unknown webcam error occurred: ' + textStatus + ':' + errorThrown + ':' + jqXHR.responseText);
            }
        }
    });
}

function handleAuditQRscan () {
  
  if (currentQrCode === null) { 
    // Do nothing
  } else {
      
    window.clearTimeout(scanTimeout);  
    scanTimeout = null;
      
    var qrData = currentQrCode.split(";");
    if (qrData.length != 11) {
      showInvalidQrAlert();
      return;
    }
    
    var district = qrData[0];
    var serialCode = qrData[2];
    var signature = qrData[4];
    
    var clips = [ ['alert_scanned_okay'] ];
    var instructions = new AudioInstructions(clips, false, true, null);
    var container = getContainer();
    var audioController = container.Resolve("audioController");
    audioController.playAudioInstructions(instructions);

    var msg = {};
    msg.type = "audit";
    msg.serialNo = serialCode;
    msg.serialSig = signature;
    msg.district = district;
    var auditSlipMessage = "msg=" + encodeURIComponent(JSON.stringify(msg));
    var auditURL = "servlet/MBBPODProxy";

    $.ajax({
      type: 'POST',
      timeout: MBB_TIMEOUT,
      url: auditURL,
      data: auditSlipMessage,
      dataType: "json",
      success: function (response) {

        // check response 
        if (response == null) {
          showAuditWarning();
        } else {
          // Check for errors.
          var error = response["ERROR"];
          if (error != null && error != "") {
            showAuditWarning();
          } else {
            showAuditSuccess();

            // Generate the Audit receipt.
            var region = districtRegionArray[district];
            var languageVal = $('#language').val();
            var fontSize = $('#fontsize').val();
            var contrast = $('#contrast').val();
            var interface = $('#interface').val();
    
            buildAudit(district, region, serialCode, signature, response);
          }
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
          showAuditFailure(NETWORK_ERROR_MESSAGE + "MBB");
      }
    });
  }
}

// Show the error warning.
function showInvalidQrAlert () {
  $('#details_pane').hide();
  $('#invalid_qr_alert').show();
}

// Hide the error warning.
function hideInvalidQrAlert () {
  $('#details_pane').show();
  $('#invalid_qr_alert').hide();
}

function alertFailure (x, t, m) {
  if (t === "timeout") {
    alertFailureToConnect();
  } else {
    alert(t);
  }
}

function showPleaseWait () {
  $('#vps_content').hide();
  $('#wait_modal_container').show();
}

function hidePleaseWait () {
  $('#vps_content').show();
  $('#wait_modal_container').hide();
}

function showMbbError (error) {

  if (error == null || error == "") {
    error = "An unknown error occurred while communicating with the MBB.";
  }

  window.clearTimeout(alertDelayedTimeout);

  $('#create_candidate_list').hide();
  $('#mbb_error_modal_container').show();
  $('#mbb_error_modal_message').html(error);
}

function hideMbbError () {
  $('#create_candidate_list').show();
  $('#mbb_error_modal_container').hide();
  $('#mbb_error_modal_message').html("");
}

function showVpsMbbTimeoutAlert () {

  window.clearTimeout(alertDelayedTimeout);

  $('#vps_content').hide();
  $('#mbb_timeout_modal_container').show();
}

// Hide the timeout warning.
function hideVpsMbbTimeoutAlert () {

  window.clearTimeout(alertDelayedTimeout);

  $('#mbb_timeout_modal_message').html("This is taking longer than expected. Please wait.");
  $('#mbb_timeout_modal_back_button').html("Cancel");

  $('#vps_content').show();
  $('#mbb_timeout_modal_container').hide();
}