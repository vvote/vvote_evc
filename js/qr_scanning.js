/**
 *   This file is part of vVote from the Victorian Electoral Commission.
 *
 *   vVote is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License.
 *
 *   vVote is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with vVote.  If not, see <http://www.gnu.org/licenses/>.
 *
 *   Contact Craig Burton   craig.burton@vec.vic.gov.au
 *
 *
 * Functions for reading and handling QR code scanning.
 * 
 * @author Peter Scheffer
 */

// Read the ballot shuffle orders from the QR code and apply accordingly.
function readPreferenceList(qrData) {

  var district = qrData[1];
  var region = qrData[2];
  var serialNumber = qrData[3];
  var dateTime = qrData[4];
  var votingCentre = qrData[5];
  var votingPreferences = qrData[6];
  var districtUncontested = qrData[7];
  var regionUncontested = qrData[8];

  var container = getContainer();
  var votingSession = container.Resolve("votingSession");
  votingSession.setDistrict(district);
  votingSession.setRegion(region);
  votingSession.setSerialCode(serialNumber);
  votingSession.setDistrictIsUncontested(districtUncontested);
  votingSession.setRegionIsUncontested(regionUncontested);
  
  var readbackSession = container.Resolve("readbackSession");
  readbackSession.setDateVoted(dateTime);
  readbackSession.setLocationVoted(votingCentre);
  readbackSession.setVote(votingPreferences);
  readbackSession.setScannedPreferencesReceipt(true);
  readbackSession.setSerialCode(serialNumber);
  
  setBallotDrawData(district, region);
  
  handlePreferencesSlipQrScanResponse();
}

// Read the ballot shuffle orders and application settings from the QR code and apply accordingly.
// Returns the serial number in the QR code.
function readCandidateList(qrData) {
    
  // If there is not the 11 data elements we're expecting, then it's not our QR code.
  if (qrData.length != 11) {
    throw "invalid_qr_code";
  }

  var district = qrData[0];
  var region = qrData[1];
  var serialCode = qrData[2];
  var shuffleOrder = qrData[3].split(":");
  var assemblyShuffleOrder = shuffleOrder[0];
  var atlShuffleOrder = shuffleOrder[1];
  var btlShuffleOrder = shuffleOrder[2];
  var signature = qrData[4];
  var language = languageOptions[qrData[5]];
  var fontSize = FONT_SIZE[qrData[6]];
  var contrast = COLOR_CONTRAST[qrData[7]];
  var userInterface = INTERFACE[qrData[8]];
  var districtUncontestedValue = qrData[9];
  var regionUncontestedValue = qrData[10];
  
  var districtUncontested = false;
  if (districtUncontestedValue == 1) {
    districtUncontested = true;
  }
  
  var regionUncontested = false;
  if (regionUncontestedValue == 1) {
    regionUncontested = true;
  }

  if (language == null || 
      fontSize == null || 
      contrast == null || 
      userInterface == null || 
      atlShuffleOrder == null || 
      btlShuffleOrder == null ||
      district == null ||
      region == null ||
      serialCode == null ||
      signature == null) {
    throw "invalid_qr_code";
  }
  
  if (!districtUncontested && assemblyShuffleOrder == null) {
    throw "invalid_qr_code";
  }

  applyQrCodeSettings(language, fontSize, contrast, userInterface);  

  var container = getContainer();
  var legislativeAssemblyBallot = container.Resolve("assembly");
  var aboveTheLineCouncilBallot = container.Resolve("atl");
  var belowTheLineCouncilBallot = container.Resolve("btl");  
  legislativeAssemblyBallot.setShuffleOrder(assemblyShuffleOrder);
  aboveTheLineCouncilBallot.setShuffleOrder(atlShuffleOrder);
  belowTheLineCouncilBallot.setShuffleOrder(btlShuffleOrder);

  var votingSession = container.Resolve("votingSession");
  votingSession.setDistrict(district);
  votingSession.setRegion(region);
  votingSession.setSerialCode(serialCode);
  votingSession.setSignature(signature);
  votingSession.setLanguage(language);
  votingSession.setDistrictIsUncontested(districtUncontested);
  votingSession.setRegionIsUncontested(regionUncontested);

  setBallotDrawData(district, region);

  var districtIsUncontested = votingSession.getDistrictIsUncontested();

  // Check for AUI option.
  if (userInterface == GVS) {
    gvsMode = true;
    tvsMode = false;
    $('#keypad').hide();
  } else if (userInterface == TVS) {
    tvsMode = true;
    gvsMode = false;
    $('#keypad').show();
  }

  return serialCode;
}

function applyQrCodeSettings (language, fontSize, contrast, userInterface) {

  var container = getContainer();
  var votingSession = container.Resolve("votingSession"); 
  
  if (fontSize == MEDIUM) {
    votingSession.setFontSize(MEDIUM);
    votingSession.setQrFontSize(MEDIUM);
    $("#select_medium_text").click();
  } else if (fontSize == LARGE) {
    votingSession.setFontSize(LARGE);
    votingSession.setQrFontSize(LARGE);
    $("#select_large_text").click();
  }

  changeLanguage(language);
  
  if (contrast == BLACK_ON_WHITE) {
    $('#black_on_white_option').click();
  } else if (contrast == WHITE_ON_BLACK) {
    $('#white_on_black_option').click();
  }
}

// Display a screen and play audio asking the voter to read back their Preference Receipt.  Gesture driven interface.
function handlePreferencesSlipQrScanResponse () {

  window.clearTimeout(alertDelayedTimeout);

  var container = getContainer();
  var visualScreenManager = container.Resolve("visualScreenManager");
  var screenFactory = container.Resolve("screenFactory");
  var currentVisualInterfaceScreen = visualScreenManager.getCurrentVisualInterfaceScreen();
  if (currentVisualInterfaceScreen != 'scan_error_visual_screen') {
    var visualView = container.Resolve("visualView");
    visualView.displaySection(currentVisualInterfaceScreen, 'scan_error_visual_screen');
  }
  var errorScreen = screenFactory.getInstance("scan_error_visual_screen");
  errorScreen.displayPreferencesSlipDetails();
}

function storeVoteInSession(data) {
  var container = getContainer();
  var readbackSession = container.Resolve("readbackSession");
  readbackSession.setVote(data);

  window.clearTimeout(alertDelayedTimeout);
}

// Display a screen and play audio asking the voter to read back their Candidate List.  Gesture driven interface.
function handleCandidateSlipQrScanResponse (data) {

  window.clearTimeout(alertDelayedTimeout);

  var container = getContainer();
  var visualScreenManager = container.Resolve("visualScreenManager");
  var currentVisualInterfaceScreen = visualScreenManager.getCurrentVisualInterfaceScreen();
  var visualView = container.Resolve("visualView");
  var screenFactory = container.Resolve("screenFactory");
  var audioScreenFactory = container.Resolve("audioScreenFactory");
  var audioScreenManager = container.Resolve("audioScreenManager");
  var audioController = container.Resolve("audioController");
  audioController.cancelRepeatInstructions();

  var readbackSession = container.Resolve("readbackSession");
  readbackSession.setScannedCandidateSlip(true);

  serialNumber = readCandidateList(data);

  // If it hasn't been voted, does it instruct the app to enter GVS, TVS, or VUI?
  if (gvsMode == true) {
    $('#keypad').hide();

    window.clearTimeout(inactivityCounterTimeout);

    // Listen for audio UI gestures.
    var touchContainer = document.getElementById('audio_only_screen');
    var hammer = new Hammer(touchContainer);
    hammer.onhold = blindInterfaceGestureTrigger;
    hammer.ontap = blindInterfaceGestureTrigger;
    hammer.ondoubletap = blindInterfaceGestureTrigger;
    hammer.ontransformstart = blindInterfaceGestureTrigger;
    hammer.ontransform = blindInterfaceGestureTrigger;
    hammer.ontransformend = blindInterfaceGestureTrigger;
    hammer.ondragstart = blindInterfaceGestureTrigger;
    hammer.ondrag = blindInterfaceGestureTrigger;
    hammer.ondragend = blindInterfaceGestureTrigger;
        
    visualView.displaySection(currentVisualInterfaceScreen, 'audio_only_screen');
    
    window.setTimeout(function() {
      audioScreenManager.execute({ 
        request: 'enterScreen', 
        data: 'audio_start_screen', 
        callback: audioController.enterBlindUiScreen 
      });
    }, 1000);
        
  } else if (tvsMode == true) {
    $('#keypad').show();

    window.clearTimeout(inactivityCounterTimeout);

    // Listen for audio UI gestures.
    var touchContainer = document.getElementById('audio_only_screen');
    var hammer = new Hammer(touchContainer);
    hammer.onhold = blindInterfaceGestureTrigger;

    visualView.displaySection(currentVisualInterfaceScreen, 'audio_only_screen');

    window.setTimeout(function() {
      audioScreenManager.execute({ 
        request: 'enterScreen', 
        data: 'tvs_entry_screen', 
        callback: audioController.enterBlindUiScreen 
      });
    }, 1000);
  } else {    
    window.setTimeout(function() {
      visualView.displaySection('triage_screen', 'language_screen');
    }, 1000);
  }
}

// Post a request to Chris' getBarcode servlet to get a QR code from a Candidate List or a Preference Receipt.
// If I don't hear back, then wait 10.5 seconds before asking again.
// If I get a 500 error then it didn't find a barcode, so ask again.
function getBarcode(callbackSuccess) {

  $.ajax({
    type: 'POST',
    timeout: SCAN_TIMEOUT,
    url: "/servlet/getBarcode",
    success: function(response){ 
      callbackSuccess(response);
    },
    error: function (jqXHR, textStatus, errorThrown) {
        console.error(textStatus + " - " + jqXHR.status);
        if (textStatus === "timeout") {
            window.clearTimeout(barcodeTimeout);
            getBarcode(callbackSuccess);
        } else if (jqXHR.status == 500) {
            window.clearTimeout(barcodeTimeout);
            getBarcode(callbackSuccess);
        } else {
            //console.info("the request has not timed out and server has not responded with valid status, resetting the QR scanner.");
            window.clearTimeout(barcodeTimeout);
            barcodeTimeout = window.setTimeout(function () { getBarcode(processQRcode); }, SCAN_TIMEOUT);
        }
    }
  });
}

// QR scan callback function.  It works like this -
// First check if it's a Preference Receipt (PR) or a Candidate List (CL). PR's have "receipt" as the first QR code data value.
// If it's a P.R then we must be doing read back.  If it has already been scanned, then ignore it, somebody left it under the scanner.
// If it hasn't been scanned, then read its data and display the "you're now in read back" screen.
// No matter which paper slip it is, we restart the webcam to scan the next QR code because the voter probably wants both scanned.
// If it's a CL, then we MAY be doing read back. We won't know until we ask MBB to start a voting session with it.
// If MBB says "StartEVM message previously sent", then we must be doing read back. Read its data and display the "you're now in read back" screen. 
// Other possibilities are - 
//  + MBB didn't reach consensus to start the session.
//  + The CL has timed out before the voter got to scan it.
//  + Other issue, eg. Network / MBB failed connection.
//  + The QR code isn't valid (it's not one of ours, or it wasn't printed correctly.)
function processQRcode(msg) {
    //console.log("qr > processQRcode: " + msg);
  // msg comes null if no code were detected
  if (msg === null) {
    // Do nothing
  } else {

    // Kill off the waiting getBarcode request.
    window.clearTimeout(barcodeTimeout);
    barcodeTimeout = null;

    var qrData = msg.split(";");
    var chosenLanguage = languageOptions[qrData[5]] == undefined ? languageOptions[1] : languageOptions[qrData[5]];
    // This might be a receipt.
    if (qrData[0] == "receipt") {

      // Check if already scanned Preference Receipt for read back. If so, ignore re-scans of it.
      var container = getContainer();
      var readbackSession = container.Resolve("readbackSession");
      var alreadyScanned = readbackSession.getScannedPreferencesReceipt();
      if (alreadyScanned == true) {
        window.clearTimeout(barcodeTimeout);
        barcodeTimeout = window.setTimeout(function () { getBarcode(processQRcode); }, SCAN_TIMEOUT);
        return;
      }

      var clips = [audioArray[chosenLanguage]['please_wait']];
      var instructions = new AudioInstructions(clips, true, true, null);
      var container = getContainer();
      var audioController = container.Resolve("audioController");
      audioController.playAudioInstructions(instructions);

      var visualScreenManager = container.Resolve("visualScreenManager");
      var currentVisualInterfaceScreen = visualScreenManager.getCurrentVisualInterfaceScreen();
      $("#" + currentVisualInterfaceScreen).show();
      
      readbackSession.setScannedPreferencesReceipt(true);
      
      // Read and store the QR code data.
      readPreferenceList(qrData);
      
      return;
      
    } else {

      // Check if already scanned Candidate List for read back. If so, ignore re-scans of it.
      var container = getContainer();
      var readbackSession = container.Resolve("readbackSession");
      var alreadyScanned = readbackSession.getScannedCandidateSlip();
      if (alreadyScanned == true) {
        window.clearTimeout(barcodeTimeout);
        barcodeTimeout = window.setTimeout(function () { getBarcode(processQRcode); }, SCAN_TIMEOUT);
        return;
      }

      var clips = [audioArray[chosenLanguage]['please_wait']];
      var instructions = new AudioInstructions(clips, true, true, null);
      var container = getContainer();
      var audioController = container.Resolve("audioController");
      audioController.playAudioInstructions(instructions);

      try {
        var district = qrData[0];
        var serialCode = qrData[2];
        var signature = qrData[4];

        // Read and store the QR code data.
        readCandidateList(qrData);

        var msg = {};
        msg.type = "startevm";
        msg.serialNo = serialCode;
        msg.serialSig = signature;
        msg.district = district;
        var startEvmMessage = "msg=" + encodeURIComponent(JSON.stringify(msg));
        var startURL = "servlet/MBBProxy";

// TODO: Remove this!
        if (using_fake_data) {

            var startURL = "response.txt";
            $.ajax({
                type: 'GET',
                timeout: MBB_TIMEOUT,
                url: startURL,
                dataType: "json",
                success: function (response) {
                    // Otherwise, we have a valid session.    
                    hideWaitingAlert(true);

                    if (response["WBB_Responses"] != null)
                    {

                        if (response["WBB_Responses"].length > 0) {
                            var mbb_error_message = response["WBB_Responses"][0]["msg"];
                            if (mbb_error_message == EVM_STARTED_ALREADY) {

                                //console.info(EVM_STARTED_ALREADY);

                                var readbackSession = container.Resolve("readbackSession");
                                readbackSession.setSerialCode(serialCode);
                                readbackSession.setScannedCandidateSlip(true);

                                var visualScreenManager = container.Resolve("visualScreenManager");
                                var currentVisualInterfaceScreen = visualScreenManager.getCurrentVisualInterfaceScreen();
                                if (currentVisualInterfaceScreen != 'scan_error_visual_screen') {
                                    var visualView = container.Resolve("visualView");
                                    visualView.displaySection(currentVisualInterfaceScreen, 'scan_error_visual_screen');
                                }

                                var screenFactory = container.Resolve("screenFactory");
                                var errorScreen = screenFactory.getInstance("scan_error_visual_screen");
                                errorScreen.displayCandidateSlipDetails();

                                return;

                                // The MBB can't make it's mind up.
                            } else if (mbb_error_message == EVM_STARTED_NO_CONSENSUS) {
                                //console.info(EVM_STARTED_NO_CONSENSUS);
                                window.clearTimeout(barcodeTimeout);
                                barcodeTimeout = window.setTimeout(function () { getBarcode(processQRcode); }, SCAN_TIMEOUT);

                                return;

                                // Candidate Lists have a time out period. This one has passed the time out period.
                            } else if (mbb_error_message == BALLOT_ALREADY_TIMED_OUT) {

                                showInvalidQrAlert();
                                return;
                            } else if (EVM_SERIAL_NO_INVALID_EXPRESSION.test(mbb_error_message)) {
                                console.error("Message validation failed - the device public key may not be authenticated by MBBs");
                                showMbbTimeoutAlert();
                                return;
                            } else if (mbb_error_message == EVM_STARTED_ALREADY_USED) {
                                showInvalidQrAlert();
                                return;
                            }
                            // No actual error responses from the MBB, just "couldn't connect to MBB"
                        } else {
                            showMbbTimeoutAlert();
                            return;
                        }
                    }

                    //Training mode errors out if there is PR and CL scan, just to avoid overlap of screens check and hide read back screen
                    var readbackSession = container.Resolve("readbackSession");
                    var alreadyScanned = readbackSession.getScannedPreferencesReceipt();
                    if (alreadyScanned == true) {
                        var visualScreenManager = container.Resolve("visualScreenManager");
                        var currentVisualInterfaceScreen = visualScreenManager.getCurrentVisualInterfaceScreen();
                        if (currentVisualInterfaceScreen == 'scan_error_visual_screen') {
                            var visualView = container.Resolve("visualView");
                            visualView.displaySection(currentVisualInterfaceScreen, INTERFACE[qrData[8]] == VIS ? 'language_screen' : 'audio_only_screen');
                        }
                    }

                    var startEvmSignature = response["sigs"];
                    var votingSession = container.Resolve("votingSession");
                    votingSession.setStartEvmSignature(startEvmSignature);
                    handleCandidateSlipQrScanResponse(qrData);
                }
            });

            return;
        }


        var visualView = container.Resolve("visualView");
        visualView.stopVideo('scan_qr_code_video');
        // Tell the user to wait while we're polling the MBB.
        showWaitingAlert();

        // Ask MBB to start an EVM session with this Candidate List.
        $.ajax({
          type: 'POST',
          timeout: MBB_TIMEOUT,
          url: startURL,
          data: startEvmMessage,
          dataType: "json",
          success: function (response) { 
              
            // Got a valid startEVM response. 

            hideWaitingAlert(true);

            var container = getContainer();
            var visualScreenManager = container.Resolve("visualScreenManager");
            var currentVisualInterfaceScreen = visualScreenManager.getCurrentVisualInterfaceScreen();
        
            $("#" + currentVisualInterfaceScreen).show();

            //console.info("Got a startEVM response: " + JSON.stringify(response));

            var error = response["ERROR"];
            if (error != null && error != "") {

              // MBB reports "unable to provide start EVM". Range of reasons follows.
              if (error == START_EVM_ERROR) {

                // Check that it's not the current session with the QR code left under the scanner.
                var votingSession = container.Resolve("votingSession");
                var currentSerialCode = votingSession.getSerialCode();
                if (currentSerialCode != null && currentSerialCode == serialCode) {
              
                  // If already voted, then start read back.      
                  var readbackSession = container.Resolve("readbackSession");
                  var voterHasScannedPreferencesReceipt = readbackSession.getScannedPreferencesReceipt();
                  readbackSession.setScannedCandidateSlip(true);
                  
                  // if it's the same serial code, and the user has scanned the PR, then play read back.
                  if (voterHasScannedPreferencesReceipt) {
                    var readbackSession = container.Resolve("readbackSession");
                    readbackSession.setSerialCode(serialCode);
                    
                    var visualScreenManager = container.Resolve("visualScreenManager");
                    var currentVisualInterfaceScreen = visualScreenManager.getCurrentVisualInterfaceScreen();
                    if (currentVisualInterfaceScreen != 'scan_error_visual_screen') {
                      var visualView = container.Resolve("visualView");
                      visualView.displaySection(currentVisualInterfaceScreen, 'scan_error_visual_screen');
                    }

                    var screenFactory = container.Resolve("screenFactory");
                    var errorScreen = screenFactory.getInstance("scan_error_visual_screen");
                    errorScreen.displayCandidateSlipDetails();
       
                    return;
                  }
                } 
                
                // if "startEVM" gives "already started" then do read back.
                if (response["WBB_Responses"] != null) {

                  if (response["WBB_Responses"].length > 0) {
                    var mbb_error_message = response["WBB_Responses"][0]["msg"];
                    if (mbb_error_message == EVM_STARTED_ALREADY) {
                      
                        //console.info(EVM_STARTED_ALREADY);
                    
                      var readbackSession = container.Resolve("readbackSession");
                      readbackSession.setSerialCode(serialCode);
                      readbackSession.setScannedCandidateSlip(true);
                    
                      var visualScreenManager = container.Resolve("visualScreenManager");
                      var currentVisualInterfaceScreen = visualScreenManager.getCurrentVisualInterfaceScreen();
                      if (currentVisualInterfaceScreen != 'scan_error_visual_screen') {
                        var visualView = container.Resolve("visualView");
                        visualView.displaySection(currentVisualInterfaceScreen, 'scan_error_visual_screen');
                      }

                      var screenFactory = container.Resolve("screenFactory");
                      var errorScreen = screenFactory.getInstance("scan_error_visual_screen");
                      errorScreen.displayCandidateSlipDetails();
                    
                      return;
                    
                    // The MBB can't make it's mind up.
                    } else if (mbb_error_message == EVM_STARTED_NO_CONSENSUS) {
                        //console.info(EVM_STARTED_NO_CONSENSUS);
                      window.clearTimeout(barcodeTimeout);
                      barcodeTimeout = window.setTimeout(function () { getBarcode(processQRcode); }, SCAN_TIMEOUT);
                      
                      return;
                    
                    // Candidate Lists have a time out period. This one has passed the time out period.
                    } else if (mbb_error_message == BALLOT_ALREADY_TIMED_OUT) {
                      
                      showInvalidQrAlert();
                      return;
                    } else if (EVM_SERIAL_NO_INVALID_EXPRESSION.test(mbb_error_message)) {
                        console.error("Message validation failed - the device public key may not be authenticated by MBBs");
                        showMbbTimeoutAlert();
                        return;
                    } else if (mbb_error_message == EVM_STARTED_ALREADY_USED) {
                        showInvalidQrAlert();
                        return;
                    }
                  // No actual error responses from the MBB, just "couldn't connect to MBB"
                  } else {
                    showMbbTimeoutAlert();
                    return;
                  }
                }

              } else {
                // try scanning the QR code again.
                window.clearTimeout(barcodeTimeout);
                barcodeTimeout = window.setTimeout(function () { getBarcode(processQRcode); }, SCAN_TIMEOUT);

                var visualView = container.Resolve("visualView");
                visualView.playVideo("scan_qr_code_video", "videos/start_qr.ogv");
                return;
              }
            }
            
            //Training mode errors out if there is PR and CL scan, just to avoid overlap of screens check and hide read back screen
            var readbackSession = container.Resolve("readbackSession");
            var alreadyScanned = readbackSession.getScannedPreferencesReceipt();
            if (alreadyScanned == true) {
                var visualScreenManager = container.Resolve("visualScreenManager");
                var currentVisualInterfaceScreen = visualScreenManager.getCurrentVisualInterfaceScreen();
                if (currentVisualInterfaceScreen == 'scan_error_visual_screen') {
                    var visualView = container.Resolve("visualView");
                    visualView.displaySection(currentVisualInterfaceScreen, INTERFACE[qrData[8]] == VIS ? 'language_screen' : 'audio_only_screen');
                }
            }

            // Otherwise, we have a valid session. Begin the voting session with the Candidate List.
            var startEvmSignature = response["sigs"];
            var votingSession = container.Resolve("votingSession");
            votingSession.setStartEvmSignature(startEvmSignature);
            handleCandidateSlipQrScanResponse(qrData);
          },
          
          // Program error. Something really wrong happened.
          error: function (jqXHR, textStatus, errorThrown) {
            hideWaitingAlert(true);
            showMbbTimeoutAlert();
          }
        });
      } catch (error) {
        // Anything else must be an invalid QR code.
        showInvalidQrAlert();
        return;
      }
    }
  }
}