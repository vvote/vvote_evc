﻿BatteryManager = (function () {
    var __battery = navigator.battery || navigator.mozBattery || navigator.webkitBattery;

    function init() {
        var _self = this;
        var _previousLevel = 0;

        this.startMonitoring = function () {
            if (__battery) {
                __battery.addEventListener("chargingchange", _alertChargingChange);
                __battery.addEventListener("levelchange", _alertLevelChange);
            }
            else
                console.warn("browser does not support battery checks.");

        };

        this.stopMonitoring = function () {
            if (__battery) {
                __battery.removeEventListener("chargingchange", _alertChargingChange);
                __battery.removeEventListener("levelchange", _alertLevelChange);
            }
            else
                console.warn("browser does not support battery checks.");
        };

        function _alertChargingChange() {
            //console.log("Battery level: " + __battery.level * 100 + "%");
            //console.log("Battery is charging: " + __battery.charging);
            //console.log("Battery is discharging: " + __battery.dischargingTime);

            if (__battery.charging == false)
                alert("The device is not connected to the power source, remove the Mini USB cable (red dot) and then connect the power cable (yellow dot), click should be heard when connecting and reconnect the Mini USB cable.");
        };

        function _alertLevelChange() {
            //console.log("Battery level: " + __battery.level * 100 + "%");
            //console.log("Battery is charging: " + __battery.charging);
            //console.log("Battery is discharging: " + __battery.dischargingTime);

            var currentLevel = __battery.level * 100;

            if (currentLevel > _previousLevel)
                _previousLevel = currentLevel;
            else if (currentLevel < 30) {
                alert("The battery charge is low, remove the Mini USB cable (red dot) and then reconnect the power cable (yellow dot), click should be heard when connecting and reconnect the Mini USB cable.");
                _previousLevel = currentLevel - 1;
            }
        };
    };

    return init;

})();